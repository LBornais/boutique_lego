<?php


namespace model;

class AccountModel
{

    static function check($firstname, $lastname, $mail, $password): bool{

        $db = \model\Model::connect();
        $sql = "SELECT mail FROM account WHERE mail = ?";

        $req = $db->prepare($sql);
        $req->execute(array($mail));

        $mailsBDD = $req->fetchAll();

        if($mailsBDD != null){
            return false;
        }


        if(strlen($lastname) < 2 || strlen($firstname) < 2 || !filter_var($mail, FILTER_VALIDATE_EMAIL) || strlen($password) <6)
            return false;

        return true;
    }

    static function signin($firstname, $lastname, $mail, $password): bool{
        if (self::check($firstname,$lastname,$mail,$password)){
            $db = \model\Model::connect();
            $sql = "INSERT INTO account (firstname, lastname, mail, password) VALUES (?, ?, ?, ?);";

            $req = $db->prepare($sql);
            $req->execute(array($firstname, $lastname, $mail, $password));
            return true;
        }
        return false;
    }

    static function login($mail, $password): array{
        $db = \model\Model::connect();
        $sql = "SELECT id, firstname, lastname, mail, password FROM account WHERE mail = ? && password = ?";

        $req = $db->prepare($sql);
        $req->execute(array($mail, $password));

        return $req->fetchAll();
    }

    static function update(){
        $db = \model\Model::connect();

        if (strlen($_POST["userfirstname"]) > 2) $firstname = htmlspecialchars($_POST["userfirstname"]);
        if (strlen($_POST["userlastname"]) > 2) $lastname = htmlspecialchars($_POST["userlastname"]);
        $mail = htmlspecialchars($_POST['usermail']);

        if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            $sql1 = "SELECT mail FROM account WHERE mail = ?";

            $req1 = $db->prepare($sql1);
            $req1->execute(array($mail));

            if ($req1->fetchAll() == null) $validMail = $mail;
        }

        if (isset($firstname)){
            $sqlFName = "UPDATE account SET firstname = ? WHERE mail = ?";
            $reqFName = $db->prepare($sqlFName);
            $reqFName->execute(array($firstname, $_SESSION['mail']));
            $_SESSION['firstname'] = $firstname;
        }

        if (isset($lastname)){
            $sqlLName = "UPDATE account SET lastname = ? WHERE mail = ?";
            $reqLName = $db->prepare($sqlLName);
            $reqLName->execute(array($lastname, $_SESSION['mail']));
            $_SESSION['lastname'] = $lastname;
        }

        if (isset($validMail)){
            $sqlMail = "UPDATE account SET mail = ? WHERE mail = ?";
            $reqMail = $db->prepare($sqlMail);
            $reqMail->execute(array($validMail, $_SESSION['mail']));
            $_SESSION['mail'] = $validMail;
        }

    }

}