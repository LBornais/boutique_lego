<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function listProducts(): array{
      $db = \model\Model::connect();

      $sql = "SELECT product.id, product.name, product.price, product.image, product.category, category.name as Cname FROM product INNER JOIN category 
                WHERE category.id = product.category";

      $req = $db->prepare($sql);
      $req->execute();

      // Retourner les résultats (type array)
      return $req->fetchAll();
  }

    static function infoProducts(int $id): array{
        $db = \model\Model::connect();

        $sql = "SELECT product.id, product.name AS Pname, product.price, product.image, product.image_alt1, product.image_alt2, product.image_alt3, product.spec, product.category, category.name as Cname 
                FROM product INNER JOIN category 
                WHERE product.category = category.id && product.id = ?";

        $req = $db->prepare($sql);
        $req->execute(array($id));

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }

    static function searchProducts(array $array): array{
        $db = \model\Model::connect();

        $str = $array['search'];

        $sql = "SELECT product.id, product.name, product.price, product.image, product.category, category.name as Cname FROM product INNER JOIN category 
                ON category.id = product.category";

        if (isset($array['category'])) {
            $c = $array['category'];
            $cpt = 0;
            foreach ($array['category'] as $c){
                if ($cpt == 0) $sql .= " WHERE (category.name = '$c'";
                else $sql .= " OR category.name = '$c'";
                $cpt++;
            }
            $sql .= ")";
        }

        $sql .= " AND product.name LIKE CONCAT('%', '$str', '%')";

        if (isset($array['order'])){
            $order = $array['order'];
            $sql .= " ORDER BY product.price $order";
        }

        $req = $db->prepare($sql);
        $req->execute();

        return $req->fetchAll();
    }

}