<?php


namespace model;


class CommentModel
{
    static function listComments(int $id): array{
        $db = \model\Model::connect();

        $sql = "SELECT DISTINCT comment.id, comment.content, comment.date, comment.id_product, comment.id_account, account.firstname AS firstname, account.lastname AS lastname FROM comment INNER JOIN product INNER JOIN account 
                WHERE id_product = ? && id_account = account.id";

        $req = $db->prepare($sql);
        $req->execute(array($id));

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }

    static function insertComment($text, $id_product, $id_account): void{
        if ($text != null){
            $db = \model\Model::connect();

            $sql = "INSERT INTO comment(content, date, id_product, id_account) VALUES (?, NOW(), ?, ?)";

            $req = $db->prepare($sql);
            $req->execute(array($text, $id_product, $id_account));
        }
    }
}