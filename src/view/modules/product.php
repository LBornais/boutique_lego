<div id="product">
    <?php foreach ($params['prod'] as $spec):?>
        <div>
            <div class="product-images">
                <img id="product-main-image" src="/public/images/<?=$spec['image']?>">
                <div id="product-miniatures" class="product-miniatures">
                    <div>
                        <img src="/public/images/<?=$spec['image']?>">
                    </div>
                    <div>
                        <img src="/public/images/<?=$spec['image_alt1']?>">
                    </div>
                    <div>
                        <img src="/public/images/<?=$spec['image_alt2']?>">
                    </div>
                    <div>
                        <img src="/public/images/<?=$spec['image_alt3']?>">
                    </div>
                </div>
            </div>

            <div class="product-infos">
                <p class="product-category"><?=$spec['Cname']?></p>
                <h1><?=$spec['Pname']?></h1>
                <p class="product-price"><?=$spec['price']?>€</p>

                <?php if ($_SESSION['granted']):?>

                <form id="product-quantity" method="post" action="/cart/add">
                    <button id="less-button" type="button">-</button>
                    <button id="product-value" type="button">1</button>
                    <button id="plus-button" type="button">+</button>

                    <input type="hidden" name="productName" value="<?= $spec['Pname']?>">
                    <input type="hidden" name="categoryName" value="<?=$spec['Cname']?>">
                    <input type="hidden" name="productPrice" id="price" value="<?= $spec['price']?>">
                    <input type="hidden" name="productImage" value="<?=$spec['image'] ?>">
                    <input type="hidden" name="productId" value="<?=$spec['id'] ?>">
                    <input type="hidden" id="sentQuantity" name="sentQuantity" value="1">


                        <input type="submit" value="Ajouter au panier">
                    <?php else: ?>
                        <div style="flex: 1"></div>
                        <button id="cart-sent"><a href="/account">Identifiez-vous pour ajouter au panier.</a></button>
                        <div style="flex: 1"></div>
                    <?php endif?>
                </form>
            </div>
        </div>

        <div>
            <div class="product-spec">
                <h2>Spécificités</h2>
                <?=$spec['spec']?>
            </div>

            <div class="product-comments">
                <h2>Avis</h2>
                    <?php if (sizeof($params['comments']) != 0):?>
                        <?php forEach($params['comments'] as $com):?>
                            <p class="product-comment-author"><?php echo $com['firstname'] . " " . $com['lastname']?></p>
                            <p>
                                <?php echo $com['content'] ?>
                            </p>
                            <br>
                        <?php endforEach;?>
                    <?php else:?>
                        <p>Il n'y a encore aucun avis sur ce produit.</p>
                        <br>
                    <?php endif;?>

                <hr/>

                <?php if ($_SESSION['granted']):?>
                <form method="post" action="/postComment">
                    <textarea style="resize: none; width: 100%; height: 100px" placeholder="Laissez un avis..." name="comment"></textarea>
                    <input type="hidden" id="id_product" name="id_product" value=<?= $spec['id']?>>
                    <input type="submit" value="Laisser un avis">

                </form>
                <?php else:?>
                <p><a href="/account">Identifiez-vous</a> pour laisser un avis.<p>
                <?php endif;?>
            </div>
        </div>

    <?php endforEach?>
</div>

<script src="/public/scripts/product.js"></script>
