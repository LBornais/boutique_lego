<?php if ($_GET['status'] == "login_fail"):?>
    <div class="box error" style="margin-left: 50px">Connexion échouée. Vérifiez vos identifiants et réessayez. </div>
<?php elseif ($_GET['status'] == "signin_suc"):?>
    <div class="box info" style="margin-left: 50px;">Inscription réussie. Vous pouvez dès à présent vous connecter. </div>
<?php elseif ($_GET['status'] == "logout_suc"):?>
    <div class="box info" style="margin-left: 50px;">Vous êtes déconnecté. À bientôt !</div>
<?php elseif ($_GET['status'] == "signin_fail"):?>
    <div class="box error" style="margin-left: 50px;">Inscription échouée. Veuillez saisir des informations valides. Il se peut que l'adresse mail renseignée soit déjà utilisée</div>
<?php endif;?>

<script src="/public/scripts/signin.js"></script>

<div id="account">

<form class="account-login" method="post" action="/account/login">

  <h2>Connexion</h2>
  <h3>Tu as déjà un compte ?</h3>

  <p>Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" />

  <input type="submit" value="Connexion" />

</form>

<form class="account-signin" method="post" action="/account/signin";">

  <h2>Inscription</h2>
  <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

  <p>Nom</p>
  <input type="text" name="userlastname" id="lastname" placeholder="Nom" />

  <p>Prénom</p>
  <input type="text" name="userfirstname" id="firstname" placeholder="Prénom" />

  <p>Adresse mail</p>
  <input type="text" name="usermail" id="mail" placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input type="password" name="userpass" id="password" placeholder="Mot de passe" />

  <p>Répéter le mot de passe</p>
  <input type="password" name="userpass" id="passwordVerif" placeholder="Mot de passe" />

  <input type="submit" value="Inscription"/>

</form>

</div>


