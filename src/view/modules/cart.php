<div id="cart">

        <?php if (count($_SESSION['cart']) > 0):?>
            <form id="cart-form" action="/cart/update" method="post">
                <?php forEach ($_SESSION['cart'] as $prod):?>

                    <div class="cart-product">
                        <div class="cart-img">
                            <img src="/public/images/<?=$prod['image']?>">
                        </div>

                        <div class="cart-name">
                            <strong class="cart-category"><?= $prod['category']?></strong>
                            <div style="flex: 1"></div>
                            <strong class="cart-title"><?= $prod['name']?></strong>
                        </div>

                        <div class="cart-quantity">
                            <p>Quantité :</p>
                            <button class="less-button" type="button">-</button>
                            <button class="product-value" type="button"><?= $prod['quantity']?></button>
                            <button class="plus-button" type="button">+</button>

                            <input type="hidden" name="id[]" value="<?=$prod['id']?>">
                            <input type="hidden" name="quantity[]" class="quantityChange" value="<?=$prod['quantity']?>">
                        </div>

                        <div class="cart-unit-price">
                            <p>Prix unitaire : <span class="cart-price"><?= $prod['price'] ?></span>€</p>
                        </div>
                    </div>
                    <hr/>

                <?php endforEach;?>

                <input type="hidden" id="update" class="update-button" value="Mettre à jour le panier">
            </form>

            <div id="total-price">
                <h2>Total : <span id="total"></span>€</h2>

                <button id="pay">Procéder au paiement</button>
            </div>

        <?php else:?>
        <div id="empty">
            <h1>Votre panier est vide...</h1>
        </div>

        <?php endif;?>
</div>

<script type="text/javascript" src="/public/scripts/cart.js"></script>