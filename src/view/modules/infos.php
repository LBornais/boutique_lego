<?php if ($_GET['status'] == "infos_suc"):?>
<div class="box info" style="margin-left: 50px">Vos informations ont bien été modifiées ! </div>
<?php endif; ?>
<div style="margin: 50px">
    <h2 style="margin 32px">Informations du compte</h2>

    <div id="account-modif">

        <form class="account-signin" method="post" action="/account/update">

            <h3>Informations personnelles</h3>

            <p>Prénom</p>
            <input type="text" name="userfirstname" id="firstname" placeholder="Prénom">

            <p>Nom</p>
            <input type="text" name="userlastname" id="lastname" placeholder="Nom">

            <p>Adresse mail</p>
            <input type="text" name="usermail" id="mail" placeholder="Adresse mail">
            <div></div>
            <input type="submit" value="Modifier mes informations"/>

        </form>

    </div>

    <div style="margin: 32px">
        <h3>Commandes</h3>
        Tu n'as pas de commande en cours.
    </div>
</div>



