<?php error_reporting(0) ?>
<nav>
    <img src="/public/images/logo.jpeg">

    <a href="/">
        Accueil
    </a>

    <a href="/store">
        Boutique
    </a>

    <?php if (!$_SESSION['granted']):?>
    <a class="account" href="/account">
        <img src="/public/images/avatar.png">
        Compte
    </a>

    <?php else: ?>
    <a class="account" href="/account/infos">
        <img src="/public/images/avatar.png">
        <?php echo $_SESSION['firstname']." ".$_SESSION['lastname']?>
    </a>

    <a href="/cart">
        Panier
    </a>

    <a href="/account/logout">
        Déconnexion
    </a>

    <?php endif ?>

</nav>
