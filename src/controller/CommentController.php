<?php


namespace controller;


class CommentController
{
    public function insertComment(): void{
        $text = htmlspecialchars($_POST['comment']);

        \model\CommentModel::insertComment($text, $_POST['id_product'], $_SESSION['id']);

        header("Location: /store/".$_POST['id_product']);
        exit();
    }

}