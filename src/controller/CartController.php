<?php


namespace controller;


use view\Template;

class CartController
{

    public function cart(): void{
        $params=[
            "title" => "Panier",
            "module" => "cart.php"
            ];

        Template::render($params);
    }

    public function add(): void{

        $newProduct = array(
          "id" => $_POST['productId'],
          "name" => $_POST['productName'],
          "price" => $_POST['productPrice'],
          "quantity" => $_POST['sentQuantity'],
          "category" => $_POST['categoryName'],
          "image" => $_POST['productImage']
        );

        $cpt=0;

        if (!isset($_SESSION['cart'])){
            $_SESSION['cart'] = array($newProduct);
        }else{
            forEach($_SESSION['cart'] as $prod){
                if (strcmp($prod['id'], $newProduct['id']) === 0){
                    if ((int) $prod['quantity'] + (int) $newProduct['quantity'] <=5)
                        $_SESSION['cart'][$cpt]['quantity'] = (string)((int)$prod['quantity'] + (int)$newProduct['quantity']);
                    else $_SESSION['cart'][$cpt]['quantity'] =5;
                    header("Location: /cart");
                    exit();
                }
                $cpt++;
            }
            $_SESSION['cart'] = array_merge($_SESSION['cart'], array($newProduct));
        }

        header("Location: /cart");
        exit();

    }

    public function update(): void{

        $quantity = $_POST['quantity'];
        $cpt = 0;

        forEach ($_SESSION['cart'] as $prod){
            if ((int)$quantity[$cpt] === 0) unset($_SESSION['cart'][$cpt]);
            else $_SESSION['cart'][$cpt]['quantity'] = $quantity[$cpt];
            $cpt++;
        }

        sort($_SESSION['cart']);

        header("Location: /cart");
        exit();
    }


}