<?php

namespace controller;

use model\CommentModel;
use model\StoreModel;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $products = StoreModel::listProducts();
    // Variables à transmettre à la vue
    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
        "product" => $products
    );


    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }

    public function product(int $id): void{
        $prod = StoreModel::infoProducts($id);
        $coms = CommentModel::listComments($id);
        $params = array(
            "title" => $id,
            "module" => "product.php",
            "prod" => $prod,
            "comments" => $coms
        );

        if ($params["prod"] == null){
            header("Location: /store");
            exit();
        }else{
            \view\Template::render($params);
        }
    }

    public function search(){
        if(count($_POST) > 1 || strlen($_POST['search']) > 0){

            $categories = \model\StoreModel::listCategories();
            $products = StoreModel::searchProducts($_POST);

            $params = array(
                "title" => "Store",
                "module" => "store.php",
                "categories" => $categories,
                "product" => $products
            );
            \view\Template::render($params);

        }else{
            $categories = \model\StoreModel::listCategories();
            $products = StoreModel::listProducts();
            // Variables à transmettre à la vue
            $params = array(
                "title" => "Store",
                "module" => "store.php",
                "categories" => $categories,
                "product" => $products
            );
            \view\Template::render($params);
        }

    }
}