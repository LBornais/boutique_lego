<?php


namespace controller;


use model\AccountModel;
use view\Template;

class AccountController
{
    public function account(): void{
        $params =[
          "title" => "Account",
            "module" => "account.php"
        ];

        Template::render($params);
    }

    public function login(): void{
        $infos = \model\AccountModel::login($_POST['usermail'], $_POST['userpass']);
        if ($infos != null){
            $_SESSION['granted'] = true;
            $_SESSION['id'] = $infos[0]['id'];
            $_SESSION['firstname'] = $infos[0]['firstname'];
            $_SESSION['lastname'] = $infos[0]['lastname'];
            $_SESSION['mail'] = $infos[0]['mail'];
            $_SESSION['password'] = $infos[0]['password'];
            header("Location: /store");
            exit();
        }
        header("Location: /account?status=login_fail");
        exit();
    }

    public function signin(): void{
        if (count($_POST) > 0){

            $firstname = htmlspecialchars($_POST['userfirstname']);
            $lastname = htmlspecialchars($_POST['userlastname']);
            $mail = htmlspecialchars($_POST['usermail']);
            $password = htmlspecialchars($_POST['userpass']);


            if (AccountModel::check($firstname, $lastname, $mail, $password)){
                \model\AccountModel::signin($firstname, $lastname, $mail, $password);
                header("Location: /account?status=signin_suc");
                exit();
            }
        }
        header("Location: /account?status=signin_fail");
        exit();
    }

    public function logout(): void{
        session_destroy();

        header("Location: /account?status=logout_suc");
        exit();
    }

    public function infos(): void{
        $params =[
          "title" => "Informations personnelles",
          "module" => "infos.php"
        ];

        Template::render($params);
    }

    public function update(): void{
        AccountModel::update();
        header("Location: /account/infos?status=infos_suc");
        exit();
    }
}