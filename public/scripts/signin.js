document.addEventListener('DOMContentLoaded', function () {

    let firstname = document.getElementById("firstname")
    let lastname = document.getElementById("lastname")
    let mail = document.getElementById("mail")
    let password = document.getElementById("password")
    let passwordVerif = document.getElementById("passwordVerif")

    const regex = new RegExp(/\S+@\S+.\S+/)

    lastname.addEventListener('change', function (){
        if (lastname.value.length >= 2){
            lastname.previousElementSibling.className = "valid"
            lastname.className = "valid"
        }else{
            lastname.previousElementSibling.className = "invalid"
            lastname.className = "invalid"
        }
    });

    firstname.addEventListener('change', function (){
        if (firstname.value.length >= 2){
            firstname.previousElementSibling.className = "valid"
            firstname.className = "valid"
        }else{
            firstname.previousElementSibling.className = "invalid"
            firstname.className = "invalid"
        }
    });

    password.addEventListener('change', function (){
        if (password.value.length > 5){
            password.previousElementSibling.className = "valid"
            password.className = "valid"
        }else{
            password.previousElementSibling.className = "invalid"
            password.className = "invalid"
        }
    });

    mail.addEventListener('change', function (){
        if (regex.test(mail.value)){
            mail.previousElementSibling.className = "valid"
            mail.className = "valid"
        }else{
            mail.previousElementSibling.className = "invalid"
            mail.className = "invalid"
        }
    });

    passwordVerif.addEventListener('change', function (){
        if (password.value === passwordVerif.value && passwordVerif.value.length > 5){
            passwordVerif.previousElementSibling.className = "valid"
            passwordVerif.className = "valid"
        }else{
            passwordVerif.previousElementSibling.className = "invalid"
            passwordVerif.className = "invalid"
        }
    });



});