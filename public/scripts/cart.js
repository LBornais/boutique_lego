document.addEventListener('DOMContentLoaded', function () {

    let total = document.getElementById("total")
    let price = document.getElementsByClassName("cart-price")

    let quantity = document.getElementsByClassName("product-value")
    let plus = document.getElementsByClassName("plus-button")
    let less = document.getElementsByClassName("less-button")


    let inputQuantity = document.getElementsByClassName("quantityChange")

    let totalPrice = 0;

    let update = document.getElementById("update")
    let newQuantity = [];


    for (let i = 0; i < price.length; i++){
        newQuantity[i] = parseInt(quantity[i].innerText)
        totalPrice += parseInt(price[i].innerText) * parseInt(quantity[i].innerText);
    }
    total.innerHTML = totalPrice.toString();

    for (let i = 0; i < quantity.length; i++) {

        if (parseInt(quantity[i].innerText) === 0) less[i].disabled = true;
        else if (parseInt(quantity[i].innerText) === 5) plus[i].disabled = true;
    }


    for (let i = 0; i < quantity.length; i++){

        less[i].addEventListener("click", function () {
            let visible = true;

            if (parseInt(quantity[i].innerText) > 1){
                quantity[i].innerText = parseInt(quantity[i].innerText) - 1;
                plus[i].disabled = false;
                less[i].disabled = false;
            }
            else if (parseInt(quantity[i].innerText) === 1){
                quantity[i].innerText = parseInt(quantity[i].innerText) - 1;
                plus[i].disabled = false;
                quantity[i].disabled = true;
            }
            else {
                plus[i].disabled = false;
                quantity[i].disabled = true;
            }

            totalPrice = 0;
            for (let i = 0; i < price.length; i++){
                totalPrice += parseInt(price[i].innerText) * parseInt(quantity[i].innerText);
            }
            total.innerHTML = totalPrice.toString();

            inputQuantity[i].value = quantity[i].innerText;

            for (let i = 0; i < quantity.length; i++){
                if(newQuantity[i] != parseInt(quantity[i].innerHTML)) visible = false;
            }

            if (!visible) update.type = "submit";
            else update.type = "hidden";

        });

        plus[i].addEventListener("click", function () {

            let visible = true;

            if (parseInt(quantity[i].innerText) === 5){
                plus[i].disabled = true;
                less[i].disabled = false;
            }
            else if (parseInt(quantity[i].innerText) === 4) {

                quantity[i].innerText = parseInt(quantity[i].innerText) + 1;
                plus[i].disabled = true;
                less[i].disabled = false;
            }
            else{
                quantity[i].innerText = parseInt(quantity[i].innerText) + 1;
                plus[i].disabled = false;
                less[i].disabled = false;
            }

            totalPrice = 0;
            for (let i = 0; i < price.length; i++){
                totalPrice += parseInt(price[i].innerText) * parseInt(quantity[i].innerText);
            }
            total.innerHTML = totalPrice.toString();

            inputQuantity[i].value = quantity[i].innerText;

            inputQuantity[i].value = quantity[i].innerText;

            for (let i = 0; i < quantity.length; i++){
                if(newQuantity[i] != parseInt(quantity[i].innerHTML)) visible = false;
            }

            if (!visible) update.type = "submit";
            else update.type = "hidden";
        });
    }

    let pay = document.getElementById("pay")

});