document.addEventListener('DOMContentLoaded', function () {

    /*--------------GESTION DE LA QUANTITE--------------*/

    let plus = document.getElementById("plus-button")
    let less = document.getElementById("less-button")
    let displayValue = document.getElementById("product-value")
    let quantity = document.getElementById("product-quantity")

    let newQuantity = document.getElementById("sentQuantity");

    let value = 1

    less.addEventListener("click", function (){
        if(value > 1 ){
            value--;
            displayValue.innerHTML = value.toString();
            if (value === 4) quantity.removeChild(quantity.lastChild);
            newQuantity.value = value;
        }
    })

    plus.addEventListener("click", function (){
        if (value < 4 ){
            value++;
            displayValue.innerHTML = value.toString();
            newQuantity.value = value;

        }else if (value === 4){
            value++;
            displayValue.innerHTML = value.toString();

            let error = document.createElement("div")
            error.id = "quantity-error"
            error.innerHTML = "Quantité maximale autorisée !"
            error.classList.add("box")
            error.classList.add("error")
            quantity.appendChild(error)
            newQuantity.value = value;
        }
    })

    /*--------------GESTION DES IMAGES--------------*/

      let imageChoice = document.getElementById("product-miniatures")
      let imagesMin = imageChoice.getElementsByTagName("img")

      function displayImage(image){
          let img = document.getElementById("product-main-image")
          img.src = image.src
      }

      for (let i in imagesMin) {
          imagesMin[i].addEventListener('click', function () {
              displayImage(imagesMin[i])
          })
      }



})