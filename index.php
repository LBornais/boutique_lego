<?php

/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

$router = new router\Router(basename(__DIR__));

session_start();

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/', 'controller\IndexController@index');

//Store
$router->get('/store', 'controller\StoreController@store');

//Produit
$router->get('/store/{:num}', 'controller\StoreController@product');

//Compte
$router->get('/account', 'controller\AccountController@account');

//Se conecter
$router->post('/account/login', 'controller\AccountController@login');

//S'enregistrer
$router->post('/account/signin', 'controller\AccountController@signin');

//Se déconnecter
$router->get('/account/logout', 'controller\AccountController@logout');

//Commenter
$router->post('/postComment', 'controller\CommentController@insertComment');

//Recherche
$router->post('/search', 'controller\StoreController@search');

//Infos perso
$router->get('/account/infos', 'controller\AccountController@infos');

//MaJ infos perso
$router->post('/account/update', 'controller\AccountController@update');

//Panier
$router->get('/cart', 'controller\CartController@cart');

//Ajouter au panier
$router->post('/cart/add', 'controller\CartController@add');

//MaJ du panier
$router->post('/cart/update', 'controller\CartController@update');

// Erreur 404
$router->whenNotFound('controller\ErrorController@error');

/** Ecoute des requêtes entrantes *********************************************/

$router->listen();
